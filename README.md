How to run code:

pyhton3 nelearn.py(or postrain.py) inputfile modelfile devfile

#dev file is optional so if you dont want to give it you can run it as

pyhton3 nelearn.py(or postrain.py) inputfile modelfile

for the taggers:

python3 neclassify.py(or postag.py) modelfile <inputfile>outputfile


PART 4

1. For POS tagger the accuracy was: 95.75772864371712
2. For entity tager:

precisioncount forB-PER: 1076
recallcount forB-PER: 1222
common for B-PER: 781
precision for B-PER: 0.7258364312267658
recall for B-PER: 0.6391162029459901
f-score for B-PER: 0.679721496953873


precisioncount forB-LOC: 1139
recallcount forB-LOC: 984
common for B-LOC: 612
precision for B-LOC: 0.5373134328358209
recall for B-LOC: 0.6219512195121951
f-score for B-LOC: 0.5765426283560999


precisioncount forB-MISC: 447
recallcount forB-MISC: 445
common for B-MISC: 190
precision for B-MISC: 0.4250559284116331
recall for B-MISC: 0.42696629213483145
f-score for B-MISC: 0.4260089686098655


precisioncount forB-ORG: 1793
recallcount forB-ORG: 1697
common for B-ORG: 1102
precision for B-ORG: 0.6146123814835471
recall for B-ORG: 0.6493812610489098
f-score for B-ORG: 0.6315186246418338


average fscore is: 0.5784479296404181

 3. For pos the accuracy for the Naive Bayes was: 92.35627727392528

 This is less than the averages perceptron scores. The reason could be that the perceptron rewards or penalizes both the classes 1. that's predicted wrongly and 2. that should have been predicted. This leads to pairwise fitting to the traning data. Now, the words feature values represent  not only how good it is for this particular class (which they do in Naive Bayes also) but also that this other class could have been predicted but us and not we have been penalized for that other class and been fitted properly.