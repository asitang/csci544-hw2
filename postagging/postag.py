# read the model file into the variables
import sys
import ast
import io
import re

def wordshape(word):
	
	wordsh=''

	prev='0'
	for letter in word:
		if letter.isalpha():
			if prev!='A' and letter.isupper():
				wordsh+='A'
				prev='A'
			elif prev!='a':
				wordsh+='a'
				prev='a'	
		if re.match('\d',letter) and prev!='d':
			wordsh+='d'
			prev='d'
		if re.match('\W',letter) and prev!='p':
			wordsh+='p'	
			prev='p'	

	return wordsh

def suffix(word):
	letter=''
	if len(word)<3:
		letter=word
	else:	
		letter=word[-3]+word[-2]+word[-1]
	return letter	

def prefix(word):
	letter=''
	if len(word)<2:
		letter=word
	else:	
		letter=word[0]+word[1]
	return letter		

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='cp1252',errors='ignore')
modelfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
#out=open('dev','w')
classavgvectors={}

while True:
	temp1=modelfile.readline().replace('\n','')
	if 'done' in temp1:
		break
	temp2=modelfile.readline()
	classavgvectors.update({temp1:ast.literal_eval(temp2)})

#out.write(str(classavgvectors.keys()))


# go thru each line in the test file 

lin=''
for line in input_stream:
	line=line.replace('\n','')
	line=line.strip(' ')
	lin=line
	line='SOL/SOL '+line+' LOL/LOL'
	wordlist=line.split(' ')
	outline=''
	predictedtag='SOL'
	for i in range(1,len(wordlist)-1):
		
		templine=''	
		templine='sh:'+wordshape(wordlist[i].split('/')[0])+' suffix:'+suffix(wordlist[i].split('/')[0])+' prevPOS:'+predictedtag+' currWORD:'+wordlist[i].split('/')[0]+' nextWORD:'+wordlist[i+1].split('/')[0]+'\n'
		#print(templine)			

		templine=templine.replace('\n','')
		wordlist2=templine.split(' ')
		
		classsums={}
	
		for classs in classavgvectors.keys():
			tempsum=0
			for word in wordlist2:
				if word in classavgvectors.get(classs).keys():
					tempsum+=classavgvectors.get(classs).get(word)
			classsums.update({classs:tempsum})
	
		#print(classavgvectors.keys())

		winnerclass='';
		winnerscore=0;
	
		for classs in classavgvectors.keys():
			winnerclass=classs
			winnerscore=classsums.get(classs)
			break

		for classs in classavgvectors.keys():
			if classsums.get(classs)>winnerscore:
				winnerclass=classs
				winnerscore=classsums.get(classs)

		predictedtag=winnerclass
		
		if i==len(wordlist)-2:
			outline+=wordlist[i].split('/')[0]+'/'+predictedtag
			#print(wordlist[i].split('/')[1])
			#print(predictedtag)
		else:
			outline+=wordlist[i].split('/')[0]+'/'+predictedtag+' '
			#print(wordlist[i].split('/')[1])
			#print(predictedtag)	
	
	#print(lin)
	print(outline)


