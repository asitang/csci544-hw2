
import sys
import re

usedev=0

if len(sys.argv)==4:
	usedev=1

def wordshape(word):
	
	wordsh=''

	prev='0'
	for letter in word:
		if letter.isalpha():
			if prev!='A' and letter.isupper():
				wordsh+='A'
				prev='A'
			elif prev!='a':
				wordsh+='a'
				prev='a'	
		if re.match('\d',letter) and prev!='d':
			wordsh+='d'
			prev='d'
		if re.match('\W',letter) and prev!='p':
			wordsh+='p'	
			prev='p'	

	return wordsh

inputfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
tempfile=open('temp','w')

for line in inputfile:
	line=line.replace('\n','')
	line=line.strip(' ')
	line='SOL/SOL/SOL '+line+' LOL/LOL/LOL'
	wordlist=line.split(' ')
	#outputfile.write(str(wordlist))
	
	
	for i in range(1,len(wordlist)-1):
		templine=''	
		templine=wordlist[i].split('/')[2]+' sh:'+wordshape(wordlist[i].split('/')[0])+' prevPOS:'+wordlist[i-1].split('/')[1]+' prevNER:'+wordlist[i-1].split('/')[2]+' currWORD:'+wordlist[i].split('/')[0]+' currPOS:'+wordlist[i].split('/')[1]+' nextPOS:'+wordlist[i+1].split('/')[1]+' nextWORD:'+wordlist[i+1].split('/')[0]+'\n'
		#templine+=wordlist[i].split('/')[2]+' sh:'+wordshape(wordlist[i].split('/')[0])+' prevPOS:'+wordlist[i-1].split('/')[1]+' currWORD:'+wordlist[i].split('/')[0]+' currPOS:'+wordlist[i].split('/')[1]+' nextPOS:'+wordlist[i+1].split('/')[1]+' nextWORD:'+wordlist[i+1].split('/')[0]+'\n'
		tempfile.write(templine)

tempfile.close()
inputfile.close()

if usedev==1:
	inputfile=open(sys.argv[3],'r', encoding='cp1252', errors='ignore')
	tempfile=open('dev','w')

	for line in inputfile:
		line=line.replace('\n','')
		line=line.strip(' ')
		line='SOL/SOL/SOL '+line+' LOL/LOL/LOL'
		wordlist=line.split(' ')
		#outputfile.write(str(wordlist))
	
	
		for i in range(1,len(wordlist)-1):
			templine=''

			templine=wordlist[i].split('/')[2]+' sh:'+wordshape(wordlist[i].split('/')[0])+' prevPOS:'+wordlist[i-1].split('/')[1]+' prevNER:'+wordlist[i-1].split('/')[2]+' currWORD:'+wordlist[i].split('/')[0]+' currPOS:'+wordlist[i].split('/')[1]+' nextPOS:'+wordlist[i+1].split('/')[1]+' nextWORD:'+wordlist[i+1].split('/')[0]+'\n'
			#templine=wordlist[i].split('/')[2]+' sh:'+wordshape(wordlist[i].split('/')[0])+' prevPOS:'+wordlist[i-1].split('/')[1]+' currWORD:'+wordlist[i].split('/')[0]+' currPOS:'+wordlist[i].split('/')[1]+' nextPOS:'+wordlist[i+1].split('/')[1]+' nextWORD:'+wordlist[i+1].split('/')[0]+'\n'
			tempfile.write(templine)

	tempfile.close()
	inputfile.close()


#perceptron code

classvectors={}
classavgvectors={}
n=0
itr=5
outputfile=open(sys.argv[2],'w')
#outputfile2=open('ner.esp.multimodel','w')
#calculate the number of lines in the training file:

inputfile=open('temp','r', encoding='cp1252', errors='ignore')
linesinfile=0
for line in inputfile:
	linesinfile+=1
inputfile.close()


BestModel={}
BestScore={}
BestOutput=''

while n!=itr:
	inputfile=open('temp','r', encoding='cp1252', errors='ignore')
	devfile=open('dev','r', encoding='cp1252', errors='ignore')
	#bestOutputTemp=''
	errorintrain=0	
	totalpredictionsintrain=0
	correctintrain=0
	correct=0
	error=0
	totalpredictions=0
	n+=1
	for line in inputfile:
		line=line.replace('\n','')
		totalpredictionsintrain+=1
		wordlist=line.split(' ')
		classname=wordlist[0]
		wordlist.remove(classname)

		if classname not in classavgvectors.keys():
			classavgvectors.update({classname:{}})
			classvectors.update({classname:{}})


		tempscoredict={}
		# initialize it
		for classs in classvectors.keys():
			tempscoredict.update({classs:0})
	
		for classs in classvectors.keys():	
			tempsum=0;	
			for word in wordlist:
				if word in classvectors.get(classs).keys():
					tempsum+=classvectors.get(classs).get(word)
				else:
					classvectors.get(classs).update({word:0})
					classavgvectors.get(classs).update({word:0}) # add the word in that class vector if not found already

			tempscoredict.update({classs:tempsum})		

		# find the winner class
		winnerclass=''; # classname is the original class
		winnerscore=0;
		for classs in classvectors.keys():
			winnerclass=classs
			winnerscore=tempscoredict.get(classs)

		for classs in classvectors.keys():
			if tempscoredict.get(classs)>winnerscore:
				winnerclass=classs
				winnerscore=tempscoredict.get(classs)


		

		if winnerclass != classname:
			errorintrain+=1
			for word in wordlist:
				classvectors.get(winnerclass).update({word:classvectors.get(winnerclass).get(word)-1})
				classvectors.get(classname).update({word:classvectors.get(classname).get(word)+1})
				classavgvectors.get(winnerclass).update({word:classavgvectors.get(winnerclass).get(word)-(itr*linesinfile-(n-1))})
				classavgvectors.get(classname).update({word:classavgvectors.get(classname).get(word)+(itr*linesinfile-(n-1))})
		else:
			correctintrain+=1			
	
	inputfile.close()			

	
	
		#print('after: '+str(classvectors))
		#print('after: '+str(classavgvectors))		
	


	# check for errorrate in the dev file:

	if usedev==1:

		for line in devfile:
			line=line.replace('\n','')
		
			wordlist=line.split(' ')
			classname=wordlist[0]
			totalpredictions+=1
			wordlist.remove(classname)


			tempscoredict={}
		
	
			for classs in classvectors.keys():	
				tempsum=0;	
				for word in wordlist:
					if word in classavgvectors.get(classs).keys():
						tempsum+=classavgvectors.get(classs).get(word)
					
				tempscoredict.update({classs:tempsum})		

			# find the winner class
			winnerclass=''; # classname is the original class
			winnerscore=0;
			for classs in classvectors.keys():
				winnerclass=classs
				winnerscore=tempscoredict.get(classs)

			for classs in classvectors.keys():
				if tempscoredict.get(classs)>winnerscore:
					winnerclass=classs
					winnerscore=tempscoredict.get(classs)
			if winnerclass != classname:
				error+=1
				#bestOutputTemp+=classname+'\n'
				#bestOutputTemp+=winnerclass+'\n'
			else:
				correct+=1
				#bestOutputTemp+=classname+'\n'
				#bestOutputTemp+=winnerclass+'\n'
	

	if usedev==1:			
		if n==1:
			print('best taken from dev')
			BestModel=classavgvectors
			BestScore=correct
			#BestOutput=bestOutputTemp
		else:
			if BestScore<correct:
				print('best taken from dev')
				BestScore=correct
				BestModel=classavgvectors
				#BestOutput=bestOutputTemp
		print(str(error)+' '+str(correct)+' '+str(totalpredictions)+'\n')
		print(str((correct/totalpredictions)*100)+' %'+'\n')
		devfile.close()

				
	else:
		if n==1:
			print('best taken from train')
			BestModel=classavgvectors
			BestScore=correctintrain
			#BestOutput=bestOutputTemp
		else:
			if BestScore<correctintrain:
				print('best taken from train')
				BestScore=correctintrain
				BestModel=classavgvectors
				#BestOutput=bestOutputTemp					
		print(str(errorintrain)+' '+str(correctintrain)+' '+str(totalpredictionsintrain)+'\n')
		print(str((correctintrain/totalpredictionsintrain)*100)+' %'+'\n')		
				
	
	
	
	
	
#print(BestOutput)
for classs in classavgvectors.keys():
	outputfile.write(str(classs)+'\n')
	outputfile.write(str(BestModel.get(classs))+'\n')

outputfile.write('done'+'\n')

"""for classs in classvectors.keys():
	outputfile2.write(str(classs)+'\n')
	outputfile2.write(str(classvectors.get(classs))+'\n')

outputfile2.write('done'+'\n')"""


#print('final model : '+str(classvectors))
#print('final model : '+str(classavgvectors))	


	

			
